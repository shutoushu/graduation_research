VANET　Simulation




〇フォルダを利用するためにあらかじめinstallしておくもの

ubunts
NS3 ネットワークシミュレータ
SUmo　交通通流シミュレータ

〇フォルダごとの説明
・longautobahn

SUMOで用いるファイル
longatobahn.sumocfgファイルをSUMOGUIで起動すると車両の動きをGUI上で確認できる
このファイルのtclファイルをNS３にインポートして用いる

・mobility

今回実験で用いたtclファイルをまとめたファイル
車両密度に応じて数種類のtclファイルが存在する

・senko

今回実験で用いた比較対象のモジュールファイル
/home/shuto/workspace/bake/source/ns-3.29/srcに保存する

・shuto

提案手法のモジュールファイル
/home/shuto/workspace/bake/source/ns-3.29/srcに保存する


〇実行方法

今回の実験ではシナリオファイルとしてns2-mobility-trace.ccファイルを用いる.
ns2-mobility-trace ファイルに各ノードの通信設定やIPアドレスの割り振り、
shuto,senkoの二つのモジュールファイルを使えるようにincludeをしておく.


実行コマンド
./waf --run "scratch/ns2-mobility-trace --traceFile=/home/shuto/mobility_20/500_2s_20.tcl --nodeNum=110 --duration=400.0 --logFile=ns2-mob.log"

ディレクトリはns-3.29で実行する
traceファイルとしてSumoで作成したtclファイルを指定する nodeNumには車両台数
duration シミュレーション時間を設定する　単位はsecond

〇注意点
シナリオで指定したノード数とシミュレーション時間をshuto,senkoそれぞれのファイルで
一致させる
シミュレーション時間　ノード数ともにdefine で定義されている


実行結果
シミュレーション時間の終了時にコマンドプロンプト上で今回の評価に必要な
速度超過車両の検出、パケットの送信数などが表示される

